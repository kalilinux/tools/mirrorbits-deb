HTML templates & third-party assets
-----------------------------------

Mirrorbits provides statistics if the query `?mirrorlist` or `?mirrorstats` is
appended at the end of the URL requested. By default, it returns an HTML page
that fetches assets (javascript and fonts) from third-parties.

Mirrorbits allows the system administrator to change this behavior though, and
serve local assets instead. Here's how to.

First, let's run the helper script that downloads all the assets needed. We
assume that we'll save those in `/srv/SITE/js`.

  mkdir /srv/SITE/js
  /usr/share/mirrorbits/contrib/localjs/fetchfiles.sh /srv/SITE/js

Then configure Mirrorbits to use it. Edit the file `/etc/mirrorbits.conf` and
set `LocalJSPath: /srv/SITE/js`.

Finally you will need to configure your web server to make sure it can serve
the files located in `/srv/SITE/js`.
